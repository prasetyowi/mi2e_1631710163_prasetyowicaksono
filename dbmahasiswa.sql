-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 02, 2018 at 12:36 PM
-- Server version: 10.1.26-MariaDB
-- PHP Version: 7.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dbmahasiswa`
--

-- --------------------------------------------------------

--
-- Table structure for table `postingan`
--

CREATE TABLE `postingan` (
  `postid` int(11) NOT NULL,
  `judul` varchar(255) NOT NULL,
  `post_full` varchar(255) NOT NULL,
  `tgl_post` date NOT NULL,
  `gambar` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `postingan`
--

INSERT INTO `postingan` (`postid`, `judul`, `post_full`, `tgl_post`, `gambar`) VALUES
(1, 'Membuat Database dan Table', 'Pertama-tama yang harus kita lakukan adalah meng-config database yang akan digunakan oleh codeigniter. Tetapi sebelumnya mari kita buat dulu database dan table sederhana menggunakan MySQL dengan perintah DDL dan DML seperti dibawah ini.', '2018-04-01', 'gambar01.jpg'),
(2, 'Mengatur settingan Database pada Codeigniter', 'Setelah sukses menjalankan perintah di atas, langkah selanjutnya adalah mengedit file database.php yang berada di dalam folder application/config/.', '2018-04-01', 'gambar02.jpg'),
(3, 'Menjalankan Library Database Secara Otomatis', 'Pada aplikasi yang dinamis, kita akan sangat sering berinteraksi dengan database. Maka sangat disarankan untuk menjalankan library database codeigniter secara otomatis.', '2018-04-01', 'gambar03.jpg');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `postingan`
--
ALTER TABLE `postingan`
  ADD PRIMARY KEY (`postid`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `postingan`
--
ALTER TABLE `postingan`
  MODIFY `postid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
