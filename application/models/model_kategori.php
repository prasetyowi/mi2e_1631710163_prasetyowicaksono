<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_kategori extends CI_Model
{

    public function __construct() 
    {
        parent::__construct();
    }

    public function get_all_categories()
    {
        // Urutkan berdasar abjad
        $this->db->order_by('kategori_name');

        $query = $this->db->get('kategori');
        return $query->result();
    }

    public function create_category()
    {
        $data = array(
            'kategori_name'          => $this->input->post('kategori_name'),
            'kategori_desc'   => $this->input->post('kategori_desc')
        );

        return $this->db->insert('kategori', $data);
    }

    // Dapatkan kategori berdasar ID
    public function get_category_by_id($id)
    {
        $query = $this->db->get_where('kategori', array('id' => $id));
        return $query->row();
    }
}
