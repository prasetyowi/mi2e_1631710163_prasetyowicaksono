<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_user extends CI_Model
{

    public function register($enc_password){
       // Array data user
       $data = array(
           'nama' => $this->input->post('nama'),
           'email' => $this->input->post('email'),
           'username' => $this->input->post('username'),
           'password' => $enc_password,
           'kodepos' => $this->input->post('kodepos'),
           'level_id' => $this->input->post('membership'),
           'gambar' => 'user.PNG'
       );

       // Insert user
       return $this->db->insert('users', $data);

    }

    // Proses login user
    public function login($username, $password){
        // Validasi
        $this->db->where('username', $username);
        $this->db->where('password', $password);

        $result = $this->db->get('users');

        if($result->num_rows() == 1){
            return $result->row(0)->user_id;
        } else {
            return false;
        }
    }

   // Mendapatkan level user
    public function get_user_level($user_id)
    {
        // Dapatkan data user berdasar $user_id
        $this->db->select('level_id');
        $this->db->where('user_id', $user_id);

        $result = $this->db->get('users');

        // print_r($result->num_rows())

        $cnt = $result->row('level_id');
        return $cnt;

    }

    public function get_user_details($user_id)
    {
        $this->db->join('levels', 'levels.level_id = users.level_id', 'left');
        $this->db->where('user_id', $user_id);

        $result = $this->db->get('users');

        if($result->num_rows() == 1){
            return $result->row(0);
        } else {
            return false;
        }
    }

    public function upload()
    {
      $config['upload_path'] = './upload';
      $config['allowed_types'] = 'jpg|png';
      $config['max_size']  = '2048';
      $config['remove_space']  = TRUE;
      
      $this->load->library('upload', $config);
      
      if ($this->upload->do_upload('input_gambar')){
        $return = array('result' => 'success', 'file' => $this->upload->data(), 'error' => '');
        return $return;
      } else {
        $return = array('result' => 'failed', 'file' => '', 'error' => $this->upload->display_errors());
        return $return;
      }
    }

    public function edit_data($where,$table)
    {    
      return $this->db->get_where($table,$where);
    }
 
    public function update_user($upload)
    {
      
      $id = $this->input->post('id_user');
      $nama = $this->input->post('nama');
      $kodepos = $this->input->post('kodepos');
      $email = $this->input->post('email');
      $username = $this->input->post('username');
      $password = md5($this->input->post('password'));
      
      $data = array(
        'nama' => $nama,
        'kodepos' => $kodepos,
        'email' => $email,
        'username' => $username,
        'password' => $password,
        'gambar' => $upload['file']['file_name']
      );
     
      $where = array(
        'user_id' => $id
      );

      $this->db->where($where);
      $this->db->update('users',$data);
      
    } 

}
