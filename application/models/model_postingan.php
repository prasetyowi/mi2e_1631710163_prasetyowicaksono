<?php
  class Model_postingan extends CI_Model {
 
    public function getPostingan($limit=FALSE, $offset = FALSE)
    {
        if($limit){
          $this->db->limit($limit,$offset);
        }

        $this->db->order_by('postingan.tgl_post','DESC');

        $query = $this->db->get('postingan');
        return $query->result();
    }

    public function get_total()
    {
      return $this->db->count_all("postingan");
    }

  public function get_single($id)
	{
  		$query = $this->db->query('select * from postingan where postid='.$id);
  		return $query;
	}

  public function upload()
  {
    $config['upload_path'] = './upload';
    $config['allowed_types'] = 'jpg|png';
    $config['max_size']  = '2048';
    $config['remove_space']  = TRUE;
    
    $this->load->library('upload', $config);
    
    if ($this->upload->do_upload('input_gambar')){
      $return = array('result' => 'success', 'file' => $this->upload->data(), 'error' => '');
      return $return;
    } else {
      $return = array('result' => 'failed', 'file' => '', 'error' => $this->upload->display_errors());
      return $return;
    }
  }

  public function insert($upload)
  {
    $this->load->helper('form');
    $this->load->library('form_validation');

    $data = array(
      'judul' => $this->input->post('input_judul'),
      'post_full' => $this->input->post('input_artikel'),
      'post_thumbnail' => $this->input->post('input_thumb_artikel'),
      'tgl_buat' => date("Y-m-d H:i:s"),
      'tgl_post' => date("Y-m-d H:i:s"),
      'author' => $this->input->post('input_author'),
      'sumber' => $this->input->post('input_sumber'),
      'gambar' => $upload['file']['file_name']
    );

    $this->db->insert('postingan', $data);
  }

  public function edit_data($where,$table)
  {    
    return $this->db->get_where($table,$where);
  }
 
  public function update_data($upload)
  {
    
    $id = $this->input->post('id');
    $judul = $this->input->post('input_judul');
    $post = $this->input->post('input_artikel');
    $post_thumb = $this->input->post('input_thumb_artikel');
    $author = $this->input->post('input_author');
    $sumber = $this->input->post('input_sumber');
    
    $data = array(
      'judul' => $judul,
      'post_full' => $post,
      'post_thumbnail' => $post_thumb,
      'author' => $author,
      'sumber' => $sumber,
      'gambar' => $upload['file']['file_name']
    );
   
    $where = array(
      'postid' => $id
    );
   
    $this->db->where($where);
    $this->db->update('postingan',$data);
  } 

  public function hapus_data($where,$table)
  {
    $this->db->where($where);
    $this->db->delete($table);
  }
 
}