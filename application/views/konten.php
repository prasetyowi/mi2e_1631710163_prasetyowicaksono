    <!-- Header -->
    <header class="masthead d-flex">
      <div class="container text-center my-auto">
        <h1 class="mb-1">Stylish Portfolio</h1>
        <h3 class="mb-5">
          <em>A Free Bootstrap Theme by Start Bootstrap</em>
        </h3>
        <a class="btn btn-primary btn-xl js-scroll-trigger" href="#about">Find Out More</a>
      </div>
      <div class="overlay"></div>
    </header>

    <!-- Services -->
    <section class="content-section bg-primary text-white text-center" id="services">
      <div class="container">
        <div class="content-section-heading">
          <h2 class="text-secondary mb-0">Post Content</h2><br><br>
        </div>
        <?php foreach($postingan->result() as $row): ?>
        <div class="row">
          <div class="col-lg-10 mx-auto">
            <h2><?php echo $row->judul; ?></h2>
            <span>Date post : <?php echo $row->tgl_post; ?></span>
            <br><br>
            <img src="upload/<?php echo $row->gambar; ?>" alt="" style="width:300px;height:200px;margin-bottom:15px;" />
            <p class="lead mb-5"><?php echo $row->post_full; ?></p>
          </div>
        </div>
        <?php endforeach; ?>
      </div>
    </section>