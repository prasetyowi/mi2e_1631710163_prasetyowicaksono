<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?><!doctype html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Stylish Portfolio - Start Bootstrap Template</title>

    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_url().'assets/vendor/bootstrap/css/bootstrap.min.css' ?>" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="<?php echo base_url().'assets/vendor/font-awesome/css/font-awesome.min.css' ?>" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url().'assets/vendor/simple-line-icons/css/simple-line-icons.css' ?>" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<?php echo base_url().'assets/css/stylish-portfolio.min.css' ?>" rel="stylesheet">

    <script>
    // Example starter JavaScript for disabling form submissions if there are invalid fields
    (function() {
      'use strict';
      window.addEventListener('load', function() {
        // Fetch all the forms we want to apply custom Bootstrap validation styles to
        var forms = document.getElementsByClassName('needs-validation');
        // Loop over them and prevent submission
        var validation = Array.prototype.filter.call(forms, function(form) {
          form.addEventListener('submit', function(event) {
            if (form.checkValidity() === false) {
              event.preventDefault();
              event.stopPropagation();
            }
            form.classList.add('was-validated');
          }, false);
        });
      }, false);
    })();
    </script>


  </head>

  <body id="page-top">
    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
      <div class="container">
        <a class="navbar-brand" href="#">Start Bootstrap</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item">
              <a class="nav-link" href="../welcome/index">Home</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="../welcome/index#about">About</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="view_post">Datatable</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="../kategori/index">Category</a>
            </li>
          <?php if(!$this->session->userdata('logged_in')) : ?>

                    <li class="nav-item" role="group" aria-label="Data baru">
                        <?php echo anchor('user/register', 'Register', array('class' => 'nav-link')); ?>
                    </li>
                    <li class="nav-item" role="group" aria-label="Data baru">
                        <?php echo anchor('user/login', 'Login ', array('class' => 'nav-link')); ?>
                    </li>

                <?php endif; ?>

                <?php if($this->session->userdata('logged_in')) : ?>
                    <li class="nav-item" role="group" aria-label="Data baru">
                        <?php echo anchor('welcome/tambah', 'Artikel Baru', array('class' => 'nav-link')); ?>
                    </li>
                    <li class="nav-item" role="group" aria-label="Data baru">
                        <?php echo anchor('kategori/create', 'Kategori Baru', array('class' => 'nav-link')); ?>
                    </li>

                    <li class="nav-item" role="group" aria-label="Data baru">
                        <?php echo anchor('user/dashboard', 'Dashboard', array('class' => 'nav-link')); ?>
                    </li>
                    <li class="nav-item" role="group" aria-label="Data baru">
                       <?php echo anchor('user/logout', 'Logout', array('class' => 'nav-link')); ?>
                    </li>
                <?php endif; ?>
                </ul>
                </div>
            </div>
            <?php if($this->session->flashdata('user_registered')): ?>
            <?php echo '<div class="alert alert-success" role="alert">'.$this->session->flashdata('user_registered').'</div>'; ?>
            <?php endif; ?>

            <?php if($this->session->flashdata('login_failed')): ?>
              <?php echo '<div class="alert alert-danger">'.$this->session->flashdata('login_failed').'</div>'; ?>
            <?php endif; ?>

            <?php if($this->session->flashdata('user_loggedin')): ?>
              <?php echo '<div class="alert alert-success">'.$this->session->flashdata('user_loggedin').'</div>'; ?>
            <?php endif; ?>

            <?php if($this->session->flashdata('not_allow')): ?>
              <?php echo '<div class="alert alert-danger">'.$this->session->flashdata('not_allow').'</div>'; ?>
            <?php endif; ?>

            <?php if($this->session->flashdata('user_loggedout')): ?>
              <?php echo '<div class="alert alert-success">'.$this->session->flashdata('user_loggedout').'</div>'; ?>
            <?php endif; ?>
        </nav>

        
        <!-- akhir Header -->