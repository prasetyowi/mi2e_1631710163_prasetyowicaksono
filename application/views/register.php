<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Stylish Portfolio - Start Bootstrap Template</title>

    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_url(). '/assets/vendor/bootstrap/css/bootstrap.min.css' ?>" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="<?php echo base_url(). '/assets/vendor/font-awesome/css/font-awesome.min.css' ?>" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url(). '/assets/vendor/simple-line-icons/css/simple-line-icons.css' ?>" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<?php echo base_url(). '/assets/css/stylish-portfolio.min.css' ?> " rel="stylesheet">

    <script>
    // Example starter JavaScript for disabling form submissions if there are invalid fields
    (function() {
      'use strict';
      window.addEventListener('load', function() {
        // Fetch all the forms we want to apply custom Bootstrap validation styles to
        var forms = document.getElementsByClassName('needs-validation');
        // Loop over them and prevent submission
        var validation = Array.prototype.filter.call(forms, function(form) {
          form.addEventListener('submit', function(event) {
            if (form.checkValidity() === false) {
              event.preventDefault();
              event.stopPropagation();
            }
            form.classList.add('was-validated');
          }, false);
        });
      }, false);
    })();
    </script>

  </head>

  <body id="page-top">

    <!-- Header -->
    <header class="masthead d-flex">
      <div class="container text-center my-auto">
        <h1 class="mb-1">Stylish Portfolio</h1>
        <h3 class="mb-5">
          <em>A Free Bootstrap Theme by Start Bootstrap</em>
        </h3>
      </div>
      <div class="overlay"></div>
    </header>

    <!-- Services -->
    <section class="content-section bg-primary text-white text-center" id="services">
      <div class="container">
          <div class="container" style="width:800px; padding-top:20px; ">
            <div class="modal-content">
              <div class="modal-header">
                <h3 style="color:#272727"><span class="fa fa-plus"></span> Pendaftaran Users</h3>
              </div>
             <?php echo form_open('user/register', array('class' => 'needs-validation', 'novalidate' => '')); ?>
               <div class="form-group">
                   <label>Nama Lengkap</label>
                   <input type="text" class="form-control" name="nama" placeholder="Nama Lengkap">
               </div>
               <div class="form-group">
                   <label>Kodepos</label>
                   <input type="text" class="form-control" name="kodepos" placeholder="Kodepos">
               </div>
               <div class="form-group">
                   <label>Email</label>
                   <input type="text" class="form-control" name="email" placeholder="Email">
               </div>
               <div class="form-group">
                   <label>Username</label>
                   <input type="text" class="form-control" name="username" placeholder="Username">
               </div>
               <div class="form-group">
                   <label>Password</label>
                   <input type="password" class="form-control" name="password" placeholder="Password">
               </div>
               <div class="form-group">
                   <label>Konfirmasi Password</label>
                   <input type="text" class="form-control" name="password2" placeholder="Konfirmasi Password">
               </div>
               <button type="submit" class="btn btn-primary btn-block">Daftar</button>
            <?php echo form_close(); ?>


    <!-- Footer -->
    <footer class="footer text-center">
      <div class="container">
        <ul class="list-inline mb-5">
          <li class="list-inline-item">
            <a class="social-link rounded-circle text-white mr-3" href="#">
              <i class="icon-social-facebook"></i>
            </a>
          </li>
          <li class="list-inline-item">
            <a class="social-link rounded-circle text-white mr-3" href="#">
              <i class="icon-social-twitter"></i>
            </a>
          </li>
          <li class="list-inline-item">
            <a class="social-link rounded-circle text-white" href="#">
              <i class="icon-social-github"></i>
            </a>
          </li>
        </ul>
        <p class="text-muted small mb-0">Copyright &copy; Your Website 2017</p>
      </div>
    </footer>

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded js-scroll-trigger" href="#page-top">
      <i class="fa fa-angle-up"></i>
    </a>

    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Custom scripts for this template -->
    <script src="js/stylish-portfolio.min.js"></script>

  </body>

</html>
