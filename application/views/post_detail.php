    <!-- Services -->
    <section class="content-section bg-primary text-white text-center" id="services">
      <div class="container">
        <div class="content-section-heading">
          <h2 class="text-secondary mb-0">Post Content</h2><br><br>
        </div>
        <?php foreach($postingan->result() as $row): ?>
        <div class="row">
          <div class="col-lg-10 mx-auto">
            <h2><?php echo $row->judul; ?></h2>
            <span>Date post : <?php echo $row->tgl_post; ?></span>
            <br><br>
            <img src="../../upload/<?php echo $row->gambar; ?>" alt="" style="width:300px;height:200px;margin-bottom:15px;" />
            <p class="lead mb-5"><?php echo $row->post_full; ?> <br><br>
            ></p>
            
          </div>
        </div>
        <?php endforeach; ?>
      </div>
    </section>