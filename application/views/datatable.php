<!DOCTYPE html>
<html lang="en">

  <head>
    <title>Stylish Portfolio - Start Bootstrap Template</title>

    <script src="<?php echo base_url(). 'assets/DataTables/js/jquery-3.1.0.js'?>"></script>
    <script src="<?php echo base_url(). 'assets/DataTables/js/jquery.dataTables.min.js'?>"></script>
    <script src="<?php echo base_url(). 'assets/DataTables/js/dataTables.bootstrap.min.js'?>"></script>


    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" />

     <!-- Custom Fonts -->
    <link href="<?php echo base_url().'assets/vendor/font-awesome/css/font-awesome.min.css' ?>" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url().'assets/vendor/simple-line-icons/css/simple-line-icons.css' ?>" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<?php echo base_url().'assets/css/stylish-portfolio.min.css' ?>" rel="stylesheet">

    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_url(). 'assets/vendor/bootstrap/css/bootstrap.min.css'?>" rel="stylesheet" type="text/css">

    <!-- Custom Fonts -->

    <link href="<?php echo base_url(). 'assets/vendor/font-awesome/css/font-awesome.min.css'?>" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url(). 'assets/vendor/simple-line-icons/css/simple-line-icons.css'?>" rel="stylesheet" type="text/css">

    <!-- Custom CSS -->
    <link href="<?php echo base_url(). 'assets/css/stylish-portfolio.min.css'?>" rel="stylesheet">


  </head>
  <body id="page-top">
    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
      <div class="container">
        <a class="navbar-brand" href="#">Start Bootstrap</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item">
              <a class="nav-link" href="index">Home
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="index#about">About</a>
            </li>
            <li class="nav-item active">
              <a class="nav-link" href="view_post">Datatable</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="kategori">Category</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">Contact</a>
            </li>
          </ul>
        </div>
      </div>
    </nav>
    <!-- Services -->
      <div class="content mt-3">
        <div class="animated fadeIn">
            <div class="row">
            <div class="col-md-12">
              <div class="card">
                <div class="card-header">
                  <strong class="card-title">Data Table</strong>
                </div>

                <div class="card-body">
                  <table id="tabel-data" class="table table-striped table-bordered" width="100%" cellspacing="0">
                    <thead>
                      <tr>
                        <th>Postid</th>
                        <th>Judul</th>
                        <th>Isi</th>
                        <th>Post Thumb</th>
                        <th>Tanggal Buat</th>
                        <th>Tanggal Post</th>
                        <th>Author</th>
                        <th>Sumber</th>
                        <th>Gambar</th>
                        <th>Edit</th>
                      </tr>
                    </thead>
                    
                    <tbody>
                    <?php foreach($postingan as $row): ?>
                      <tr>
                        <td><?php echo $row->postid; ?></td>
                        <td><?php echo $row->judul; ?></td>
                        <td><?php echo $row->post_full; ?></td>
                        <td><?php echo $row->post_thumbnail; ?></td>
                        <td><?php echo $row->tgl_buat; ?></td>
                        <td><?php echo $row->tgl_post; ?></td>
                        <td><?php echo $row->author; ?></td>
                        <td><?php echo $row->sumber; ?></td>
                        <td><?php echo $row->gambar; ?></td>
                        <td>
                            <a class="btn btn-default" style="background-color:#36baff;" href="edit/<?php echo $row->postid ?>" target="_self">Update</a>
                            <a class="btn btn-default" style="background-color:#ff4b4b;" href="hapus/<?php echo $row->postid ?>" target="_self" title="Hapus Data" onclick="return confirm('Apakah Anda benar-benar akan menghapus data no. <?php echo $row->postid ?> ?')">Hapus</a>
                        </td>
                      </tr>
                      <?php endforeach; ?>
                    </tbody>
                    
                  </table>
                </div>
              </div>
            </div>
        </div><!-- .animated -->
      </div>
    </div><!-- .content -->

  
    <!-- Footer -->
    <footer class="footer text-center">
      <div class="container">
        <p class="text-muted small mb-0">Copyright &copy; Your Website 2017</p>
      </div>
    </footer>

  <script>
    $(document).ready(function(){
        $('#tabel-data').DataTable();
    });
  </script>


  </body>

</html>
