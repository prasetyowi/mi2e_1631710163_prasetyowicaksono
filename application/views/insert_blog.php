    <!-- Header -->
    <header class="masthead d-flex">
      <div class="container text-center my-auto">
        <h1 class="mb-1">Stylish Portfolio</h1>
        <h3 class="mb-5">
          <em>A Free Bootstrap Theme by Start Bootstrap</em>
        </h3>
      </div>
      <div class="overlay"></div>
    </header>

    <!-- Services -->
    <section class="content-section bg-primary text-white text-center" id="services">
      <div class="container">
        <div class="content-section-heading">
          <h2 class="text-secondary mb-0">Insert Post</h2><br><br>
        </div>
          <div class="container" style="width:800px; padding-top:20px; ">
            <div class="modal-content">
              <div class="modal-header">
                <h3 style="color:#272727"><span class="fa fa-plus"></span> Tambah artikel</h3>
                  
              </div>
              <?php $this->form_validation->set_error_delimiters('<div class="alert alert-warning" role="alert">', '</div>'); ?>
            <?php echo validation_errors(); ?>

            <?php echo (isset( $upload_error)) ? '<div class="alert alert-warning" role="alert">' .$upload_error. '</div>' : ''; ?>

              <?php echo form_open('welcome/tambah', array('enctype'=>'multipart/form-data')); ?>

                <div class="modal-body">
                  <div class="form-group">
                    <label style="color:black" for="validationCustom05">Judul : </label>
                    <input name="input_judul" type="text" class="form-control" placeholder="judul artikel...">
                  </div>
                  <div class="form-group">
                    <label style="color:black">Isi artikel : </label>
                    <textarea name="input_artikel" class="form-control" rows="5"></textarea>
                  </div>
                  <div class="form-group">
                    <label style="color:black">Thumb artikel : </label>
                    <textarea name="input_thumb_artikel" class="form-control"></textarea>
                  </div>
                  <div class="form-group">
                    <label style="color:black">Author : </label>
                    <input name="input_author" type="text" class="form-control" placeholder="Author...">
                  </div>
                  <div class="form-group">
                    <label style="color:black">Sumber : </label>
                    <input name="input_sumber" type="text" class="form-control" placeholder="Sumber...">
                  </div>
                  <div class="form-group">
                    <label style="color:black">Gambar : </label>
                    <input name="input_gambar" type="file" class="form-control">
                  </div>
                <input type="submit" class="btn btn-primary" name="submit" value="Simpan">
                </div>
              </form>
            </div>
          </div>