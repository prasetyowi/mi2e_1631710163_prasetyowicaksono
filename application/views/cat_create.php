
  <body id="page-top">
    <!-- Services -->
    <section class="content-section bg-primary text-white text-center" id="services">
      <div class="container">
        <div class="content-section-heading">
          <h2 class="text-secondary mb-0">Insert Post</h2><br><br>
        </div>
        <section>
          <div class="container">
            <div class="row">
              <div class="col-lg-8 offset-lg-2">
                
                <?php    
                  $this->form_validation->set_error_delimiters('<div class="alert alert-warning" role="alert">', '</div>');
                ?>
                <?php echo validation_errors(); ?>

                <?php echo (isset( $upload_error)) ? '<div class="alert alert-warning" role="alert">' .$upload_error. '</div>' : ''; ?>

                <?php echo form_open( 'kategori/create', array('class' => 'needs-validation', 'novalidate' => '') ); ?>

                <div class="form-group">
                  <label for="cat_name">Nama Kategori</label>
                  <input type="text" class="form-control" name="kategori_name" value="<?php echo set_value('kategori_name') ?>" required>
                  <div class="invalid-feedback">Isi judul dulu gan</div>
                </div>
                <div class="form-group">
                  <label for="text">Deskripsi</label>
                  <input type="text" class="form-control" name="kategori_desc" value="<?php echo set_value('kategori_desc') ?>" required>
                  <div class="invalid-feedback">Isi deskripsinya dulu gan</div>
                </div>
                <button id="submitBtn" type="submit" class="btn btn-primary">Simpan</button>
              </form>
            </div>
          </div>
        </div>
      </section>