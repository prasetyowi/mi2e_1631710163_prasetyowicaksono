    <header class="masthead d-flex">
      <div class="container text-center my-auto">
        <h1 class="mb-1">Stylish Portfolio</h1>
        <h3 class="mb-5">
          <em>A Free Bootstrap Theme by Start Bootstrap</em>
        </h3>
      </div>
      <div class="overlay"></div>
    </header>

        <!-- Page Content -->
    <div class="container">

    <br>
      <!-- Project One -->
      <div class="row" style="margin-top:35px;margin-bottom:35px;">
        <?php foreach($postingan as $row): ?>
        <div class="col-md-7">
          <a href="#">
            <img class="img-fluid rounded mb-3 mb-md-0" src="<?php echo base_url(). 'upload/'?><?php echo $row->gambar; ?>" style="width:600px;" alt="">
          </a>
        </div>
        <div class="col-md-5">
          <h3><?php echo $row->judul; ?></h3>
          <p><?php echo $row->post_full; ?></p>
          <a class="btn btn-primary" href="welcome/detail/<?php echo $row->postid; ?>">View Project</a>
          <a class="btn btn-primary" href="welcome/edit/<?php echo $row->postid; ?>">Update</a>
          <a class="btn btn-primary" href="welcome/delete/<?php echo $row->postid; ?>">Delete</a>
        </div>
        <br><br><br><br>
      <?php endforeach; ?>
      <?php        
        if (isset($links)) {            
          echo $links;        
        }        
      ?>
      </div>
     
      <!-- /.row -->
      <hr>
    </div>
    <!-- /.container -->

    <!-- about -->
    <section class="content-section bg-primary text-white text-center" id="about">
      <div class="container text-center">
        <div class="row">
          <div class="col-lg-10 mx-auto">
            <h2>Selamat Datang di Portofolio Saya !</h2>
            <p class="lead mb-5">Perkenalkan nama Saya adalah Prasetyo Wicaksono. Saya adalah mahasiswa Politeknik Negeri Malang, 
            jurusan Teknologi Informasi prodi D3 Manajemen Informatika. Saya sekarang menduduki kelas MI-2E. Saya di sini membuat website 
            untuk memenuhi tugas matakuliah Framework.</p>
            
          </div>
        </div>
      </div>
    </section>

    <!-- Callout -->
    <section class="callout">
      <div class="container text-center">
        <h2 class="mx-auto mb-5">Tambah Artikel Keinginanmu</h2>
        <a class="btn btn-primary btn-xl" href="tambah">Start Now!</a>
      </div>
    </section>
