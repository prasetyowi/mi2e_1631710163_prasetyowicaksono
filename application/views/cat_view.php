
    <section class="jumbotron text-center">
      <div class="container">
        <h1 class="jumbotron-heading"><?php echo $page_title ?></h1>
        
        <p>
          <?php echo anchor('kategori/create', 'Buat Kategori Baru', array('class' => 'btn btn-primary')); ?>
        </p>
      </div>
    </section>

    <?php if( !empty($categories) ) : ?>
    <div class="album py-5 bg-light">
      <div class="container">
        <div class="row">

          <?php
            // Kita looping data dari controller
            foreach ($categories as $key) :
          ?>

          <div class="col-md-4">
            <!-- Kita format tampilan blog dalam bentuk card -->
            <!-- https://getbootstrap.com/docs/4.0/components/card/ -->
            <div class="card mb-4 box-shadow border-0">
                            
              <div class="card-body">

                <!-- Batasi cuplikan konten dengan substr sederhana -->
                <h5><?php echo substr($key->kategori_name, 0, 40) ?></h5>
                <p class="card-text"><?php echo substr($key->kategori_desc, 0, 20) ?></p>
                
                <div class="d-flex justify-content-between align-items-center">
                  <div class="btn-group">
                    <!-- Untuk link detail -->
                    <a href="<?php echo base_url(). 'kategori/artikel/' . $key->id ?>" class="btn btn-outline-secondary">Baca</a>
                    <a href="<?php echo base_url(). 'kategori/edit/' . $key->id ?>" class="btn btn-outline-secondary">Edit</a>
                  </div>
                </div>
              </div>
              
            </div>
          </div>
          <?php endforeach; ?>

        </div>
      </div>
    </div>
    <?php else : ?>
    <p>Belum ada data bosque.</p>
    <?php endif; ?>