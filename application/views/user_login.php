<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Stylish Portfolio - Start Bootstrap Template</title>

    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_url().'assets/vendor/bootstrap/css/bootstrap.min.css' ?>" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="<?php echo base_url().'assets/vendor/font-awesome/css/font-awesome.min.css' ?>" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url().'assets/vendor/simple-line-icons/css/simple-line-icons.css' ?>" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<?php echo base_url().'assets/css/stylish-portfolio.min.css' ?>" rel="stylesheet">

  </head>

  <body id="page-top">

    <?php if($this->session->flashdata('user_registered')): ?>
    <?php echo '<div class="alert alert-success" role="alert">'.$this->session->flashdata('user_registered').'</div>'; ?>
    <?php endif; ?>

    <?php if($this->session->flashdata('login_failed')): ?>
    <?php echo '<p class="alert alert-danger">'.$this->session->flashdata('login_failed').'</p>'; ?>
    <?php endif; ?>

    <?php if($this->session->flashdata('user_loggedout')): ?>
    <?php echo '<p class="alert alert-success">'.$this->session->flashdata('user_loggedout').'</p>'; ?>
    <?php endif; ?>

    <?php if(!$this->session->userdata('logged_in')) : ?>

     <div class="btn-group" role="group" aria-label="Data baru">
         <?php echo anchor('user/register', 'Register', array('class' => 'btn btn-outline-light')); ?>
         <?php echo anchor('user/login', 'Login', array('class' => 'btn btn-outline-light')); ?>
     </div>

    <?php endif; ?>

    <?php if($this->session->userdata('logged_in')) : ?>
    <div class="btn-group" role="group" aria-label="Data baru">
       <?php echo anchor('welcome/tambah', 'Artikel Baru', array('class' => 'btn btn-outline-light')); ?>
       <?php echo anchor('user/logout', 'Logout', array('class' => 'btn btn-outline-light')); ?>
   </div>
  <?php endif; ?>

    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
      <div class="container">
        <a class="navbar-brand" href="#">Start Bootstrap</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item active">
              <a class="nav-link" href="index">Home
                <span class="sr-only">(current)</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="index#about">About</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="view_post">Datatable</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="kategori">Category</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="user/register">Register</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="logout">Logout</a>
            </li>
          </ul>
        </div>
      </div>
    </nav>
    <!-- Header -->
    <header class="masthead d-flex">
      <div class="container text-center my-auto">
        <h1 class="mb-1">Stylish Portfolio</h1>
        <h3 class="mb-5">
          <em>A Free Bootstrap Theme by Start Bootstrap</em>
        </h3>
      </div>
      <div class="overlay"></div>
    </header>

        <!-- Page Content -->
    <div class="container">

    <br>
      <!-- Project One -->
      <div class="row" style="margin-top:35px;margin-bottom:35px;">
        <?php foreach($postingan as $row): ?>
        <div class="col-md-7">
          <a href="#">
            <img class="img-fluid rounded mb-3 mb-md-0" src="<?php echo base_url(). 'upload/'?><?php echo $row->gambar; ?>" style="width:600px;" alt="">
          </a>
        </div>
        <div class="col-md-5">
          <h3><?php echo $row->judul; ?></h3>
          <p><?php echo $row->post_full; ?></p>
          <a class="btn btn-primary" href="welcome/detail/<?php echo $row->postid; ?>">View Project</a>
          <a class="btn btn-primary" href="welcome/edit/<?php echo $row->postid; ?>">Update</a>
          <a class="btn btn-primary" href="welcome/delete/<?php echo $row->postid; ?>">Delete</a>
        </div>
        <br><br><br><br>
      <?php endforeach; ?>
      <?php        
        if (isset($links)) {            
          echo $links;        
        }        
      ?>
      </div>
     
      <!-- /.row -->
      <hr>
    </div>
    <!-- /.container -->

    <!-- about -->
    <section class="content-section bg-primary text-white text-center" id="about">
      <div class="container text-center">
        <div class="row">
          <div class="col-lg-10 mx-auto">
            <h2>Selamat Datang di Portofolio Saya !</h2>
            <p class="lead mb-5">Perkenalkan nama Saya adalah Prasetyo Wicaksono. Saya adalah mahasiswa Politeknik Negeri Malang, 
            jurusan Teknologi Informasi prodi D3 Manajemen Informatika. Saya sekarang menduduki kelas MI-2E. Saya di sini membuat website 
            untuk memenuhi tugas matakuliah Framework.</p>
            
          </div>
        </div>
      </div>
    </section>

    <!-- Callout -->
    <section class="callout">
      <div class="container text-center">
        <h2 class="mx-auto mb-5">Tambah Artikel Keinginanmu</h2>
        <a class="btn btn-primary btn-xl" href="welcome/tambah">Start Now!</a>
      </div>
    </section>
    
    <!-- Map -->
    <section id="contact" class="map">
      <iframe width="100%" height="100%" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=Twitter,+Inc.,+Market+Street,+San+Francisco,+CA&amp;aq=0&amp;oq=twitter&amp;sll=28.659344,-81.187888&amp;sspn=0.128789,0.264187&amp;ie=UTF8&amp;hq=Twitter,+Inc.,+Market+Street,+San+Francisco,+CA&amp;t=m&amp;z=15&amp;iwloc=A&amp;output=embed"></iframe>
      <br/>
      <small>
        <a href="https://maps.google.com/maps?f=q&amp;source=embed&amp;hl=en&amp;geocode=&amp;q=Twitter,+Inc.,+Market+Street,+San+Francisco,+CA&amp;aq=0&amp;oq=twitter&amp;sll=28.659344,-81.187888&amp;sspn=0.128789,0.264187&amp;ie=UTF8&amp;hq=Twitter,+Inc.,+Market+Street,+San+Francisco,+CA&amp;t=m&amp;z=15&amp;iwloc=A"></a>
      </small>
    </section>

    <!-- Footer -->
    <footer class="footer text-center">
      <div class="container">
        <ul class="list-inline mb-5">
          <li class="list-inline-item">
            <a class="social-link rounded-circle text-white mr-3" href="#">
              <i class="icon-social-facebook"></i>
            </a>
          </li>
          <li class="list-inline-item">
            <a class="social-link rounded-circle text-white mr-3" href="#">
              <i class="icon-social-twitter"></i>
            </a>
          </li>
          <li class="list-inline-item">
            <a class="social-link rounded-circle text-white" href="#">
              <i class="icon-social-github"></i>
            </a>
          </li>
        </ul>
        <p class="text-muted small mb-0">Copyright &copy; Your Website 2017</p>
      </div>
    </footer>

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded js-scroll-trigger" href="#page-top">
      <i class="fa fa-angle-up"></i>
    </a>

    <!-- Bootstrap core JavaScript -->
    <script src="<?php echo base_url().'assets/vendor/jquery/jquery.min.js'?>"></script>
    <script src="<?php echo base_url().'vendor/bootstrap/js/bootstrap.bundle.min.js'?>"></script>

    <!-- Plugin JavaScript -->
    <script src="<?php echo base_url().'vendor/jquery-easing/jquery.easing.min.js'?>"></script>

    <!-- Custom scripts for this template -->
    <script src="<?php echo base_url().'js/stylish-portfolio.min.js'?>"></script>

  </body>

</html>
