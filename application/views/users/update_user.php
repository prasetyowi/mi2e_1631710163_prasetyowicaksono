    <!-- Services -->
    <section class="content-section bg-primary text-white text-center" id="services">
      <div class="container">
          <div class="container" style="width:800px; padding-top:20px; ">
            <div class="modal-content">
              <div class="modal-header">
                <h3 style="color:#272727"><span class="fa fa-plus"></span> Pendaftaran Users</h3>
              </div>
            <?php foreach($users as $u) { ?>
             <?php echo form_open('User/user_update', array('class' => 'needs-validation', 'novalidate' => '', 'enctype'=>'multipart/form-data')); ?>
               <div class="form-group">
                    <label style="color:black">User ID : </label>
                    <input name="id_user" type="text" class="form-control" placeholder="Userid" readonly="" value="<?php echo $u->user_id ?>" >
               </div>
               <div class="form-group">
                   <label style="color:black;">Nama Lengkap</label>
                   <input type="text" class="form-control" name="nama" placeholder="Nama Lengkap" value="<?php echo $u->nama ?>">
               </div>
               <div class="form-group">
                   <label style="color:black;">Kodepos</label>
                   <input type="text" class="form-control" name="kodepos" placeholder="Kodepos" value="<?php echo $u->kodepos ?>">
               </div>
               <div class="form-group">
                   <label style="color:black;">Email</label>
                   <input type="text" class="form-control" name="email" placeholder="Email" value="<?php echo $u->email ?>">
               </div>
               <div class="form-group">
                   <label style="color:black;">Username</label>
                   <input type="text" class="form-control" name="username" placeholder="Username" value="<?php echo $u->username ?>">
               </div>
               <div class="form-group">
                   <label style="color:black;">Password</label>
                   <input type="password" class="form-control" name="password" placeholder="Password" >
               </div>
               <div class="form-group">
                   <label style="color:black;">Konfirmasi Password</label>
                   <input type="password" class="form-control" name="password2" placeholder="Konfirmasi Password">
               </div>
                <div class="form-group">
                      <label style="color:black">Gambar : </label>
                      <input name="input_gambar" type="file" class="form-control">
                </div>
                <?php } ?>
                <div class="modal-footer">
                    <input type="submit" class="btn btn-primary" name="submit" value="Simpan">
                    <a class="btn btn-primary" href="../dashboard" ><span class="fa fa-arrow-left"></span>  Kembali</a>
                </div>
          </div>
               