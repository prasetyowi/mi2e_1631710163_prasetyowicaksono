    <!-- Services -->
    <section class="content-section bg-primary text-white text-center" id="services">
      <div class="container">
          <div class="container" style="width:800px; padding-top:20px; ">
            <div class="modal-content">
              <div class="modal-header">
                <h3 style="color:#272727"><span class="fa fa-plus"></span> Pendaftaran Users</h3>
              </div>
             <?php echo form_open('user/register', array('class' => 'needs-validation', 'novalidate' => '')); ?>
               <div class="form-group">
                   <label style="color:black;">Nama Lengkap</label>
                   <input type="text" class="form-control" name="nama" placeholder="Nama Lengkap">
               </div>
               <div class="form-group">
                   <label style="color:black;">Kodepos</label>
                   <input type="text" class="form-control" name="kodepos" placeholder="Kodepos">
               </div>
               <div class="form-group">
                   <label style="color:black;">Email</label>
                   <input type="text" class="form-control" name="email" placeholder="Email">
               </div>
               <div class="form-group">
                   <label style="color:black;">Username</label>
                   <input type="text" class="form-control" name="username" placeholder="Username">
               </div>
               <div class="form-group">
                   <label style="color:black;">Password</label>
                   <input type="password" class="form-control" name="password" placeholder="Password">
               </div>
               <div class="form-group">
                   <label style="color:black;">Konfirmasi Password</label>
                   <input type="password" class="form-control" name="password2" placeholder="Konfirmasi Password">
               </div>
               <div class="form-group">
              <label for="">Pilih Paket Membership</label>
              <div class="form-check">
                  <input class="form-check-input" type="radio" name="membership" id="goldmember" value="2" checked>
                  <label class="form-check-label" for="goldmember" style="color:black;">Gold Member</label>
              </div>
              <div class="form-check">
                  <input class="form-check-input" type="radio" name="membership" id="silvermember" value="3">
                  <label class="form-check-label" for="silvermember" style="color:black;">Silver Member</label>
                  <br><br>
              </div>
          </div>
               <button type="submit" class="btn btn-primary btn-block">Daftar</button>
            <?php echo form_close(); ?>
