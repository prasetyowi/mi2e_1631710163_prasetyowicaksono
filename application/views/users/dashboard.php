<br><br>
<div class="container">
   <div class="py-5 text-center">
       <h2>Selamat datang <?php echo $user->nama ?> <span class="badge badge-secondary"><?php echo $user->nama_level ?></span></h2>
   </div>
   <div class="row" style="margin-top:35px;margin-bottom:35px;margin-left:200px">
        <div class="col-md-5">
           <img class="img-fluid rounded mb-3 mb-md-0" src="<?php echo base_url(). 'upload/'?><?php echo $user->gambar; ?>" style="width:300px;height:256px" alt="">
        </div>
        <div class="col-md-5" style="margin-top:20px">
          <h3>Data Diri </h3>
          <p>Nama : <?php echo $user->nama; ?></p>
          <p>Username : <?php echo $user->username; ?></p>
          <p>Email : <?php echo $user->email; ?></p>
          <a class="btn btn-primary" style="width:150px;height:40px" href="edit/<?php echo $user->user_id; ?>">Update</a>
        </div>
      </div>
</div>