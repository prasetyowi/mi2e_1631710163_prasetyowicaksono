
    <!-- Services -->
    <section class="content-section bg-primary text-white text-center" id="services">
      <div class="container">
        <div class="content-section-heading">
          <h2 class="text-secondary mb-0">Insert Post</h2><br><br>
        </div>
          <div class="container" style="width:800px; padding-top:20px; ">
            <div class="modal-content">
              <div class="modal-header">
                <h3 style="color:#272727"><span class="fa fa-plus"></span> Tambah artikel</h3>
                  
              </div>
              <?php foreach($postingan as $post) { ?>
              <div class="modal-body">
              <?php echo form_open('Welcome/update', array('enctype'=>'multipart/form-data')); ?>
                  <div class="form-group">
                    <label style="color:black">Post ID : </label>
                    <input name="id" type="text" class="form-control" placeholder="judul artikel..." readonly="" value="<?php echo $post->postid ?>" >
                  </div>
                  <div class="form-group">
                    <label style="color:black">Judul : </label>
                    <input name="input_judul" type="text" class="form-control" placeholder="judul artikel..." value="<?php echo $post->judul ?>" required>
                  </div>
                  <div class="form-group">
                    <label style="color:black">Isi artikel : </label>
                    <textarea class="form-control" rows="5" name="input_artikel" id="comment" required><?php echo $post->post_full ?></textarea>
                  </div>
                  <div class="form-group">
                    <label style="color:black">Thumbnail artikel : </label>
                    <textarea class="form-control" rows="5" name="input_thumb_artikel" id="comment" required><?php echo $post->post_thumbnail ?></textarea>
                  </div>
                  <div class="form-group">
                    <label style="color:black">Author : </label>
                    <input name="input_author" type="text" class="form-control" placeholder="Author..." value="<?php echo $post->author ?>" required>
                  </div>
                  <div class="form-group">
                    <label style="color:black">Sumber : </label>
                    <input name="input_sumber" type="text" class="form-control" placeholder="Sumber..." value="<?php echo $post->sumber ?>" required>
                  </div>
                  <div class="form-group">
                    <label style="color:black">Gambar : </label>
                    <input name="input_gambar" type="file" class="form-control">
                    <label style="color:black"><?php echo $post->gambar ?></label>
                  </div>
                <?php } ?>
                  <div class="modal-footer">
                    <input type="submit" class="btn btn-primary" name="submit" value="Simpan">
                    <a class="btn" href="welcome"><span class="fa fa-arrow-left"></span>  Kembali</a>
                  </div>
              </form>
              </div>