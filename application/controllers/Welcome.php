<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$data['page_title'] = 'List Artikel';

		$limit_per_page = 4;

		$start_index = ( $this->uri->segment(3) ) ? $this->uri->segment(3) : 0;
		
		$total_records = $this->model_postingan->get_total();

		if ($total_records > 0) {        
			$data["all_artikel"] = $this->model_postingan->getPostingan($limit_per_page, $start_index);

			$config['base_url'] = base_url().'welcome/index/';    
			$config['total_rows'] = $total_records;    
			$config['per_page'] = $limit_per_page;    
			$config["uri_segment"] = 3;        

			$this->pagination->initialize($config);

			$data["links"] = $this->pagination->create_links();
		}

		$this->load->model('model_postingan');
		$data['postingan'] = $this->model_postingan->getPostingan();
		$this->load->view('templates/header');
		$this->load->view('welcome_message', $data);
		$this->load->view('templates/footer');
	}

	public function detail($id)
	{
		$this->load->model('model_postingan');
		$data['postingan'] = $this->model_postingan->get_single($id);
		$this->load->view('templates/header');
		$this->load->view('post_detail', $data);
		$this->load->view('templates/footer');
	}

	public function view_post()
	{

		if($this->session->userdata('level') == 1)
		{
			$this->load->model('model_postingan');
		    $data['postingan'] = $this->model_postingan->getPostingan();
		    $this->load->view('templates/header');
			$this->load->view('datatable', $data);
			$this->load->view('templates/footer');

		}
		else if($this->session->userdata('level') == 2)
		{
			$this->load->model('model_postingan');
		    $data['postingan'] = $this->model_postingan->getPostingan();
		    $this->load->view('templates/header');
			$this->load->view('datatable', $data);
			$this->load->view('templates/footer');

		}
		else
		{
			$this->session->set_flashdata('not allow','Hanya member yang bisa masuk !!');
			redirect('welcome/index');
		}
		
	}

	public function tambah()
	{
		//cek level user apakah admin atau tidak, halaman ini hanya bisa diakses oleh admin
		if(!$this->session->userdata('logged_in'))
		{
			$this->session->set_flashdata('not allow','Hanya member yang bisa masuk !!');
			redirect('welcome/index');
		}
		else
		{
			if($this->session->userdata('level') == 1)
			{
				$this->form_validation->set_rules('input_judul', 'Judul', 'required',
				array(
					'required' 		=> 'kolom %s tidak boleh kosong!'
				));

				$this->form_validation->set_rules('input_artikel', 'Artikel', 'required|min_length[10]',
					array(
						'required' 		=> 'kolom %s tidak boleh kosong!',
						'min_length' 	=> 'Karakter %s belum mencapai minimum'
					));
				$this->form_validation->set_rules('input_thumb_artikel', 'Thumb_Artikel', 'required|min_length[10]',
					array(
						'required' 		=> 'kolom %s tidak boleh kosong!',
						'min_length' 	=> 'Karakter %s belum mencapai minimum'
					));
				$this->form_validation->set_rules('input_author', 'Author', 'required',
					array(
						'required' 		=> 'kolom %s tidak boleh kosong!'
					));

				$this->form_validation->set_rules('input_sumber', 'Sumber', 'required',
					array(
						'required' 		=> 'kolom %s tidak boleh kosong!'
					));
				
				if ($this->form_validation->run() === FALSE)
			    {
			    	$this->load->view('templates/header');
			        $this->load->view('insert_blog');
			        $this->load->view('templates/footer');
			    } else {
					$this->load->model('model_postingan');
					$data = array();

					if ($this->input->post('submit'))
					 {
						$upload = $this->model_postingan->upload();

						if ($upload['result'] == 'success') 
						{
							$this->model_postingan->insert($upload);
							redirect('Welcome/tambah');
						} else {
							$data['message'] = $upload['error'];
						}
					}

					$this->load->view('templates/header');
					$this->load->view('insert_blog', $data);
					$this->load->view('templates/footer');
				
				}
			}
			else if($this->session->userdata('level') == 2)
			{
				$this->form_validation->set_rules('input_judul', 'Judul', 'required',
				array(
					'required' 		=> 'kolom %s tidak boleh kosong!'
				));

				$this->form_validation->set_rules('input_artikel', 'Artikel', 'required|min_length[10]',
					array(
						'required' 		=> 'kolom %s tidak boleh kosong!',
						'min_length' 	=> 'Karakter %s belum mencapai minimum'
					));
				$this->form_validation->set_rules('input_thumb_artikel', 'Thumb_Artikel', 'required|min_length[10]',
					array(
						'required' 		=> 'kolom %s tidak boleh kosong!',
						'min_length' 	=> 'Karakter %s belum mencapai minimum'
					));
				$this->form_validation->set_rules('input_author', 'Author', 'required',
					array(
						'required' 		=> 'kolom %s tidak boleh kosong!'
					));

				$this->form_validation->set_rules('input_sumber', 'Sumber', 'required',
					array(
						'required' 		=> 'kolom %s tidak boleh kosong!'
					));
				
				if ($this->form_validation->run() === FALSE)
			    {
			    	$this->load->view('templates/header');
			        $this->load->view('insert_blog');
			        $this->load->view('templates/footer');
			    } else {
					$this->load->model('model_postingan');
					$data = array();

					if ($this->input->post('submit'))
					 {
						$upload = $this->model_postingan->upload();

						if ($upload['result'] == 'success') 
						{
							$this->model_postingan->insert($upload);
							redirect('Welcome/tambah');
						} else {
							$data['message'] = $upload['error'];
						}
					}
					$this->load->view('templates/header');
					$this->load->view('insert_blog', $data);
					$this->load->view('templates/footer');
					
				}
			}
			else
			{
				$this->session->set_flashdata('not allow','Hanya member yang bisa masuk !!');
				redirect('welcome/index');
			}
		}
	}

	public function upload()
  	{
	    $config['upload_path'] = './upload';
	    $config['allowed_types'] = 'jpg|png';
	    $config['max_size']  = '2048';
	    $config['remove_space']  = TRUE;
	    
	    $this->load->library('upload', $config);
	    
	    if ($this->upload->do_upload('input_gambar')){
	      $return = array('result' => 'success', 'file' => $this->upload->data(), 'error' => '');
	      return $return;
	    } else {
	      $return = array('result' => 'failed', 'file' => '', 'error' => $this->upload->display_errors());
	      return $return;
	    }
	}

	public function edit($id)
	{
		$where = array('postid' => $id);
		$data['postingan'] = $this->model_postingan->edit_data($where,'postingan')->result();
		$this->load->view('templates/header');
		$this->load->view('update_blog',$data);
		$this->load->view('templates/footer');
	}

	public function update()
	{
		$this->load->model('model_postingan');
		$data = array();

		if ($this->input->post('submit'))
		{
			$upload = $this->model_postingan->upload();

			if ($upload['result'] == 'success') 
			{
				$this->model_postingan->update_data($upload);
				redirect('Welcome');
			} else {
				$data['message'] = $upload['error'];
			}
		}
	}

	public function hapus($id){
		$where = array('postid' => $id);
		$this->model_postingan->hapus_data($where,'postingan');
		redirect('Welcome');
	}

}
