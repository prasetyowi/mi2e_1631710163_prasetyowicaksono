<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Kategori extends CI_Controller{
	public function __construct()
	{
		parent::__construct();

		// Load custom helper applications/helpers/MY_helper.php

		// Load semua model yang kita pakai
		$this->load->model('model_postingan');
		$this->load->model('model_kategori');
	}

	public function index() 
	{
		if($this->session->userdata('level') == 1)
		{
			// Judul Halaman
			$data['page_title'] = 'List Kategori';

			// Dapatkan semua kategori
			$data['categories'] = $this->model_kategori->get_all_categories();
			$this->load->view('templates/header');
			$this->load->view('cat_view', $data);
			$this->load->view('templates/footer');
		}
		else
		{
			$this->session->set_flashdata('not allow','Hanya member yang bisa masuk !!');
			redirect('welcome/index');
		}
		
	}
	
	public function create() 
	{
		if($this->session->userdata('level') == 1)
		{
			// Judul Halaman
		$data['page_title'] = 'Buat Kategori';

		// Kita butuh helper dan library berikut
		$this->load->helper('form');
		$this->load->library('form_validation');

		// Form validasi untuk Nama Kategori
		$this->form_validation->set_rules(
			'kategori_name',
			'Nama Kategori',
			'required|is_unique[kategori.kategori_name]',
			array(
				'required' => 'Isi %s donk, males amat.',
				'is_unique' => 'Judul ' . $this->input->post('title') . ' sudah ada bosque.'
			)
		);

		if($this->form_validation->run() === FALSE){
			$this->load->view('templates/header');
			$this->load->view('cat_create', $data);
			$this->load->view('templates/footer');
		} else {
			$this->model_kategori->create_category();
			redirect('post_detail',$data);
		}
		}
		else
		{
			$this->session->set_flashdata('not allow','Hanya member yang bisa masuk !!');
			redirect('welcome/index');
		}
	}

	// Menampilkan semua artikel dalam kategori yang dipilih
	public function artikel($id) 
	{

		// Menampilkan judul berdasar nama kategorinya
		$data['page_title'] = $this->model_kategori->get_category_by_id($id)->kategori_name;

		// Dapatkan semua artikel dalam kategori ini
		$data['postingan'] = $this->model_postingan->get_single($id);

		// Kita gunakan view yang sama pada controller Blog
		$this->load->view('templates/header');
		$this->load->view('post_detail', $data);
		$this->load->view('templates/footer');
	}
}
