<?php
defined('BASEPATH') or exit('No direct script access allowed');

class User extends CI_Controller{

    public function __construct()
    {
        parent::__construct();
                
        $this->load->library('form_validation');
        $this->load->helper('MY');
        $this->load->model('model_user');
    }

    // Register user
    public function register(){
        $data['page_title'] = 'Pendaftaran User';

        $this->form_validation->set_rules('nama', 'Nama', 'required');
        $this->form_validation->set_rules('username', 'Username', 'required|is_unique[users.username]');
        $this->form_validation->set_rules('email', 'Email', 'required|is_unique[users.email]');
        $this->form_validation->set_rules('password', 'Password', 'required');
        $this->form_validation->set_rules('password2', 'Konfirmasi Password', 'matches[password]');

        if($this->form_validation->run() === FALSE){
            $this->load->view('templates/header');
            $this->load->view('users/register', $data);
            $this->load->view('templates/footer');
        } else {
            // Encrypt password
            $enc_password = md5($this->input->post('password'));

            $this->model_user->register($enc_password);

            // Set message
            $this->session->set_flashdata('user_registered', 'Anda telah teregistrasi.');

            redirect('welcome');
        }
    }

    // Log in user
    public function login(){
        $data['page_title'] = 'Log In';

        $this->form_validation->set_rules('username', 'Username', 'required');
        $this->form_validation->set_rules('password', 'Password', 'required');

        if($this->form_validation->run() === FALSE){
            $this->load->view('templates/header');
            $this->load->view('users/login', $data);
            $this->load->view('templates/footer');
        } else {
            
            // Get username
            $username = $this->input->post('username');
            // Get & encrypt password
            $password = md5($this->input->post('password'));

            // Login user
            $user_id = $this->model_user->login($username, $password);

            if($user_id){
                // Buat session
                $level = $this->model_user->get_user_level($user_id);

                $user_data = array(
                    'user_id' => $user_id,
                    'username' => $username,
                    'level' => $level,
                    'logged_in' => true
                );

                $this->session->set_userdata($user_data);

                // Set message
                $this->session->set_flashdata('user_loggedin', 'Anda sudah login');

                redirect('user/dashboard');

            } else {
                // Set message
                $this->session->set_flashdata('login_failed', 'Login invalid');

                redirect('user/login');
            }       
        }
    }

    // Log user out
    public function logout(){
        // Unset user data
        $this->session->unset_userdata('logged_in');
        $this->session->unset_userdata('user_id');
        $this->session->unset_userdata('username');

        // Set message
        $this->session->set_flashdata('user_loggedout', 'Anda sudah log out');

        redirect('user/login');
    }

    // Fungsi Dashboard
    public function dashboard()
    {
        // Must login
        if(!$this->session->userdata('logged_in')) 
            redirect('user/login');

        $user_id = $this->session->userdata('user_id');

        // Dapatkan detail dari User
        $data['user'] = $this->model_user->get_user_details( $user_id );

        // Load view
        $this->load->view('templates/header', $data, FALSE);
        $this->load->view('users/dashboard', $data, FALSE);
        $this->load->view('templates/footer', $data, FALSE);
    }

    public function dashboard_gold()
    {
        // Must login
        if(!$this->session->userdata('logged_in')) 
            redirect('user/login');

        $user_id = $this->session->userdata('user_id');

        // Dapatkan detail dari User
        $data['user'] = $this->model_user->get_user_details( $user_id );

        // Load view
        $this->load->view('templates/header', $data, FALSE);
        $this->load->view('users/dashboard_gold', $data, FALSE);
        $this->load->view('templates/footer', $data, FALSE);
    }

    public function upload()
    {
        $config['upload_path'] = './upload';
        $config['allowed_types'] = 'jpg|png';
        $config['max_size']  = '2048';
        $config['remove_space']  = TRUE;
        
        $this->load->library('upload', $config);
        
        if ($this->upload->do_upload('input_gambar')){
          $return = array('result' => 'success', 'file' => $this->upload->data(), 'error' => '');
          return $return;
        } else {
          $return = array('result' => 'failed', 'file' => '', 'error' => $this->upload->display_errors());
          return $return;
        }
    }

    public function edit($id)
    {
        $where = array('user_id' => $id);
        $data['users'] = $this->model_user->edit_data($where,'users')->result();
        $this->load->view('templates/header');
        $this->load->view('users/update_user',$data);
        $this->load->view('templates/footer');
    }

    public function user_update()
    {
        $this->load->model('model_user');
        $data = array();

        if ($this->input->post('submit'))
        {
            $upload = $this->model_user->upload();

            if ($upload['result'] == 'success') 
            {
                $this->model_user->update_user($upload);
                redirect('user/dashboard');
            } else {
                $data['message'] = $upload['error'];
            }
        }
    }

}